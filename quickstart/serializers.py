from rest_framework import serializers

from quickstart.models import Car

class CarSerializer(serializers.ModelSerializer):

    class Meta: 
        model = Car
        fields = ('name', 'color', 'brand')